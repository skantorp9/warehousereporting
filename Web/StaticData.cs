﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web
{
    public static class StaticData
    {
        public static Dictionary<int, int[]> SpetializationIdsBySpecialtyId { get; set; } = new Dictionary<int, int[]>();

        public static Dictionary<int, int[]> GroupIdsBySpetializationId { get; set; } = new Dictionary<int, int[]>();

        public static Dictionary<int, int[]> StudentIdsByGroupId { get; set; } = new Dictionary<int, int[]>();
    }
}
