﻿$("#updateCurrencies").on('click', function () {
    UpdateCurrencies();
});

function UpdateCurrencies() {
    $("#updSuccess").hide();
    $("#updFail").hide();
    $.ajax({
        type: 'POST',
        url: "/Currency/UpdateCurrencies",
        dataType: 'json',
        contentType: 'application/json',
        success: function (data) {
            if (data) {
                $("#updSuccess").show();
            }
            else {
                $("#updFail").show();
            }
        }
    });
}


function EditGood(id) {
    window.location = "/Goods/Edit?id=" + id;
}
