﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Models
{
    public class WarehouseListViewModel
    {
        public List<WarehouseViewModel> Warehouses { get; set; } = new List<WarehouseViewModel>();
    }

    public class WarehouseViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public List<int> GoodIds { get; set; } = new List<int>();
    }
}
