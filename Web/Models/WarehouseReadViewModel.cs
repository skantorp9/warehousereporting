﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Models
{
    public class WarehouseReadViewModel
    {
        public int Id { get; set; }
        public int? CurrencyId { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public double? GoodsSum { get; set; }
        public List<WarehouseGoodReadViewModel> Goods { get; set; } = new List<WarehouseGoodReadViewModel>();
        public List<SelectListItem> Currencies { get; set; } = new List<SelectListItem>();
    }

    public class WarehouseGoodReadViewModel
    {
        public string Name { get; set; }
        public int Amount { get; set; }
        public string Price { get; set; }
    }
}
