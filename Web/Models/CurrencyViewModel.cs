﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Models
{
    public class CurrencyViewModel
    {
        public string Name { get; set; }
        public double ExchangeRate { get; set; }
        public string Code { get; set; }
    }
}
