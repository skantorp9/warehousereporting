﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Models
{
    public class CurrenciesListViewModel
    {
        public bool AreCurrenciesUpdated { get; set; }
        public List<CurrencyViewModel> Currencies { get; set; } = new List<CurrencyViewModel>();
    }
}
