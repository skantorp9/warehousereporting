﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Models
{
    public class WarehouseEditViewModel
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public List<WarehouseGoodEditViewModel> Goods { get; set; } = new List<WarehouseGoodEditViewModel>();
    }

    public class WarehouseGoodEditViewModel
    {
        private static int _nextId;

        public WarehouseGoodEditViewModel()
        {
            Id = --_nextId;
        }

        public int Id { get; set; }
        public double Price { get; set; }
        public int GoodId { get; set; }
        public List<SelectListItem> GoodsDropDownList { get; set; } = new List<SelectListItem>();
        public int Amount { get; set; }
    }
}
