﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Models
{
    public class WarehouseSaveModel
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public List<WarehouseGoodSaveModel> Goods { get; set; } = new List<WarehouseGoodSaveModel>();
    }
    public class WarehouseGoodSaveModel
    {
        public int? Id { get; set; }
        public int GoodId { get; set; }
        public int Amount { get; set; }
    }
}
