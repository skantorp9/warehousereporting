﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Models
{
    public class GoodListViewModel
    {
        public List<GoodViewModel> Goods { get; set; } = new List<GoodViewModel>();
    }
    public class GoodViewModel
    {
        public int Id { get; set; }
        public int Code { get; set; }
        public string Name { get; set; }
        public string Category { get; set; }
        public string Price { get; set; }
    }
    public class GoodEditViewModel
    {
        public int? Id { get; set; }
        public double Price { get; set; }
        public string Name { get; set; }
        public int CategoryId { get; set; }
        public int CurrencyId { get; set; }
        public List<SelectListItem> Categories { get; set; } = new List<SelectListItem>();
        public List<SelectListItem> Currencies { get; set; } = new List<SelectListItem>();
    }
    public class SaveGoodModel
    {
        public int? Id { get; set; }
        public double Price { get; set; }
        public string Name { get; set; }
        public int CategoryId { get; set; }
        public int CurrencyId { get; set; }
    }
}
