﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BusinessObjects.Enums;
using Microsoft.AspNetCore.Mvc;
using Services;
using Services.DTOs;
using Services.RequestDTOs;
using Web.Models;

namespace Web.Controllers
{
    public class WarehousesController : Controller
    {
        private readonly IDbService _dbService;
        public WarehousesController(IDbService dbService)
        {
            _dbService = dbService;
        }
        public IActionResult List()
        {
            var listModel = new WarehouseListViewModel();
            listModel.Warehouses = _dbService.GetWarehouseIds()
                .Select(x => _dbService.GetWarehouseById(x))
                .Select(x => new WarehouseViewModel
                {
                    Id = x.Id,
                    Address = x.Address,
                    Name = x.Name,
                }).ToList();
            return View(listModel);
        }

        public IActionResult Edit(int? id = null)
        {
            var warehouseEditViewModel = new WarehouseEditViewModel();

            if (id.HasValue)
            {
                var warehouse = _dbService.GetWarehouseById(id.Value);
                warehouseEditViewModel.Address = warehouse.Address;
                warehouseEditViewModel.Id = id.Value;
                warehouseEditViewModel.Name = warehouse.Name;
                warehouseEditViewModel.Goods = warehouse.Goods
                    .Select(delegate (WarehouseGoodDto x)
                    {
                        var currentGood = _dbService.GetGoodById(x.GoodId);

                        return new WarehouseGoodEditViewModel
                        {
                            Id = x.Id,
                            Price = currentGood.Price,
                            GoodId = x.GoodId,
                            Amount = x.Amount,
                            GoodsDropDownList = _dbService.GetGoodIds()
                .Select(x => _dbService.GetGoodById(x)).Select(x => new Microsoft.AspNetCore.Mvc.Rendering.SelectListItem
                {
                    Value = x.Id.ToString(),
                    Text = x.Name
                }).ToList()
                        };
                    }).ToList();
            }

            return View(warehouseEditViewModel);
        }

        [HttpPost]
        public IActionResult Save([FromBody]WarehouseSaveModel warehouseSaveModel)
        {
            var saveDto = new SaveWarehouseDto
            {
                Id = warehouseSaveModel.Id,
                Address = warehouseSaveModel.Address,
                Name = warehouseSaveModel.Name,
                Goods = warehouseSaveModel.Goods.Select(x => new WarehouseGoodSaveDto
                {
                    Id = x.Id,
                    Amount = x.Amount,
                    GoodId = x.GoodId
                }).ToArray()
            };

            try
            {
                _dbService.SaveWarehouse(saveDto);
            }
            catch
            {
                return Json(false);
            }
            return Json(true);
        }

        public ActionResult GetGood(WarehouseGoodEditViewModel warehouseGoodEditViewModel)
        {
            warehouseGoodEditViewModel.GoodsDropDownList = _dbService.GetGoodIds()
                .Select(x => _dbService.GetGoodById(x)).Select(x => new Microsoft.AspNetCore.Mvc.Rendering.SelectListItem
                {
                    Value = x.Id.ToString(),
                    Text = x.Name
                }).ToList();
            return PartialView("_Good", warehouseGoodEditViewModel);
        }

        [HttpPost]
        public IActionResult Read([FromBody]WarehouseReadViewModel readModel)
        {
            CurrencyDto selectedCurrency = null;
            if (readModel.CurrencyId.HasValue)
            {
                selectedCurrency = _dbService.GetCurrencyById(readModel.CurrencyId.Value);
                readModel.GoodsSum = 0;
            }

            var warehouse = _dbService.GetWarehouseById(readModel.Id);
            readModel.Name = warehouse.Name;
            readModel.Address = warehouse.Address;
            readModel.Goods = warehouse.Goods
                .Select(delegate (WarehouseGoodDto x)
                {
                    var currentGood = _dbService.GetGoodById(x.GoodId);
                    var currency = _dbService.GetCurrencyById(currentGood.CurrencyId);
                    var priceString = $"{Math.Round(currentGood.Price * x.Amount, 4)} {currency.Code}";
                    if (selectedCurrency != null)
                    {
                        var currentPrice = currentGood.Price * selectedCurrency.ExchangeRate * x.Amount / currency.ExchangeRate;
                        priceString = $"{Math.Round(currentPrice, 4)} {selectedCurrency.Code}";
                        readModel.GoodsSum += currentPrice;
                    }

                    return new WarehouseGoodReadViewModel
                    {
                        Price = priceString,
                        Name = currentGood.Name,
                        Amount = x.Amount
                    };
                }).ToList();
            readModel.Currencies = _dbService.GetCurrencyIds().Select(x => _dbService.GetCurrencyById(x))
                .Select(x => new Microsoft.AspNetCore.Mvc.Rendering.SelectListItem
                {
                    Text = x.Code,
                    Value = x.Id.ToString()
                }).ToList();

            return PartialView(readModel);
        }

        public IActionResult Delete(int id)
        {
            try
            {
                _dbService.DeleteWarehouse(id);
            }
            catch
            {
                return Json(false);
            }
            return Json(true);
        }
    }
}