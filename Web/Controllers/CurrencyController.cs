﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using BusinessObjects.Enums;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Services;
using Services.RequestDTOs;
using Web.Models;

namespace Web.Controllers
{
    public class CurrencyController : Controller
    {
        private readonly IDbService _dbService;
        public CurrencyController(IDbService dbService)
        {
            _dbService = dbService;
        }
        public IActionResult Index()
        {
            var currencyIds = _dbService.GetCurrencyIds();

            var currenciesListViewModel = new CurrenciesListViewModel();
            
            foreach(var currencyId in currencyIds)
            {
                var currency = _dbService.GetCurrencyById(currencyId);
                currenciesListViewModel.AreCurrenciesUpdated = currency.UpdateDate.Day >= DateTime.Now.Day;
                currenciesListViewModel.Currencies.Add(new CurrencyViewModel
                {
                    Code=currency.Code,
                    ExchangeRate=currency.ExchangeRate,
                    Name=currency.Name
                });
            }

            return View(currenciesListViewModel);
        }
        
        public async Task<bool> UpdateCurrencies()
        {
            var currencyAPIs = await GetAPICurrencies();

            var currencies = currencyAPIs
                .Where(x => x.cc == CurrencyCode.Dollar || x.cc == CurrencyCode.Hryvnia || x.cc == CurrencyCode.Euro)
                .Select(x => new SaveCurrencyRequestDto
                {
                    Code = x.cc,
                    UAHExchangeRate = x.rate
                }).ToArray();

            try
            {
                _dbService.UpdateCurrencies(currencies);
            }
            catch
            {
                return false;
            }

            return true;
        }
      
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        async Task<List<CurrencyAPIModel>> GetAPICurrencies()
        {
            List<CurrencyAPIModel> currencies = new List<CurrencyAPIModel>();
            try
            {

                HttpClient client = new HttpClient();
                HttpResponseMessage response = await client.GetAsync("https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json");
                string currenciesString = "";
                if (response.IsSuccessStatusCode)
                {
                    currenciesString = await response.Content.ReadAsStringAsync();
                }
                var format = "dd/MM/yyyy"; 
                var dateTimeConverter = new IsoDateTimeConverter { DateTimeFormat = format };
                currencies = JsonConvert.DeserializeObject<List<CurrencyAPIModel>>(currenciesString, dateTimeConverter);

                
            }
            catch(Exception e)
            {

            }

            return currencies;
        }
    }
}
