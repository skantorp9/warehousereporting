﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Services;
using Web.Models;

namespace Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly IDbService _dbService;
        public HomeController(IDbService dbService)
        {
            _dbService = dbService;
        }
        public IActionResult Index()
        {
            var defaultCurrencyId = _dbService.GetCurrencyIds().First();
            var defaultCurrency = _dbService.GetCurrencyById(defaultCurrencyId);
            var viewModel = new DefaultPageViewModel
            {
                AreCurrenciesUpdated = defaultCurrency.UpdateDate.Day >= DateTime.Now.Day
            };
            return View(viewModel);
        }
    }
}