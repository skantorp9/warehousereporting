﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BusinessObjects.Enums;
using Microsoft.AspNetCore.Mvc;
using Services;
using Services.DTOs;
using Services.RequestDTOs;
using Web.Models;

namespace Web.Controllers
{
    public class GoodsController : Controller
    {
        private readonly IDbService _dbService;
        public GoodsController(IDbService dbService)
        {
            _dbService = dbService;
        }
        public IActionResult List()
        {
            var goodListViewModel = new GoodListViewModel
            {
                Goods = _dbService.GetGoodIds().Select(x => _dbService.GetGoodById(x)).Select(delegate (GoodDto x)
                {
                    var currency = _dbService.GetCurrencyById(x.CurrencyId);
                    return new GoodViewModel
                    {
                        Id = x.Id,
                        Name = x.Name,
                        Price = $"{Math.Round(x.Price, 4)} {currency.Code}",
                        Category = ((CategoryId)x.CategoryId).ToString(),
                        Code = x.BarCodeNumber
                    };
                }).ToList()
            };

            return View(goodListViewModel);
        }

        public IActionResult Edit(int? id = null)
        {
            var categories = _dbService.GetCategories();
            var editViewModel = new GoodEditViewModel
            {
                Categories = categories
                .Select(x => new Microsoft.AspNetCore.Mvc.Rendering.SelectListItem
                {
                    Text = x.Name,
                    Value = x.Id.ToString()
                }).ToList(),
                Currencies = _dbService.GetCurrencyIds().Select(x => _dbService.GetCurrencyById(x))
                .Select(x => new Microsoft.AspNetCore.Mvc.Rendering.SelectListItem
                {
                    Text = x.Code,
                    Value = x.Id.ToString()
                }).ToList()
            };
            if (id.HasValue)
            {
                var good = _dbService.GetGoodById(id.Value);

                editViewModel.Id = good.Id;
                editViewModel.CategoryId = good.CategoryId;
                editViewModel.CurrencyId = good.CurrencyId;
                editViewModel.Name = good.Name;
                editViewModel.Price = good.Price;
            }

            return View(editViewModel);
        }

        [HttpPost]
        public ActionResult Save([FromBody]SaveGoodModel saveGoodModel)
        {
            var saveModel = new SaveGoodDto
            {
                Id = saveGoodModel.Id,
                CategoryId = saveGoodModel.CategoryId,
                CurrencyId = saveGoodModel.CurrencyId,
                Name = saveGoodModel.Name,
                Price = saveGoodModel.Price
            };

            try
            {
                _dbService.SaveGood(saveModel);
            }
            catch (Exception e)
            {
                return Json(false);
            }
            return Json(true);
        }
    }
}