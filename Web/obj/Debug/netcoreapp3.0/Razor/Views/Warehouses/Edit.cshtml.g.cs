#pragma checksum "D:\mine\Partial_proj\Web\Views\Warehouses\Edit.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "b34da6825d634f65e909886811fb662c6b548538"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Warehouses_Edit), @"mvc.1.0.view", @"/Views/Warehouses/Edit.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "D:\mine\Partial_proj\Web\Views\_ViewImports.cshtml"
using Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "D:\mine\Partial_proj\Web\Views\_ViewImports.cshtml"
using Web.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"b34da6825d634f65e909886811fb662c6b548538", @"/Views/Warehouses/Edit.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"74b0619e1a302f0598271da1847e697c39d57b88", @"/Views/_ViewImports.cshtml")]
    public class Views_Warehouses_Edit : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<WarehouseEditViewModel>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 2 "D:\mine\Partial_proj\Web\Views\Warehouses\Edit.cshtml"
  
    ViewData["Title"] = "Edit";
    Layout = "~/Views/Shared/_Layout.cshtml";

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n");
#nullable restore
#line 7 "D:\mine\Partial_proj\Web\Views\Warehouses\Edit.cshtml"
Write(Html.HiddenFor(x => x.Id));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n\r\n<h4>Name</h4>\r\n");
#nullable restore
#line 10 "D:\mine\Partial_proj\Web\Views\Warehouses\Edit.cshtml"
Write(Html.TextBoxFor(x => x.Name));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n<h4>Address</h4>\r\n");
#nullable restore
#line 12 "D:\mine\Partial_proj\Web\Views\Warehouses\Edit.cshtml"
Write(Html.TextBoxFor(x => x.Address));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n\r\n<h4>Goods</h4>\r\n<table class=\"table\">\r\n    <tr>\r\n        <th>Name of the good</th>\r\n        <th>Amount</th>\r\n    </tr>\r\n</table>\r\n<div id=\"goodsContainer\">\r\n");
#nullable restore
#line 22 "D:\mine\Partial_proj\Web\Views\Warehouses\Edit.cshtml"
     foreach (var good in Model.Goods)
    {
        await Html.RenderPartialAsync("~/Views/Warehouses/_Good.cshtml", good);
    }

#line default
#line hidden
#nullable disable
            WriteLiteral(@"</div>
<a onclick=""OnAddGoodLinkClick()"">
    <svg width=""24px"" height=""24px"" viewBox=""0 0 24 24"" version=""1.1"">
        <g id=""Symbols"" stroke=""none"" stroke-width=""1"" fill=""none"" fill-rule=""evenodd"">
            <g id=""ic-/-plus"" fill-rule=""nonzero"">
                <polygon id=""Path"" points=""19 13 13 13 13 19 11 19 11 13 5 13 5 11 11 11 11 5 13 5 13 11 19 11""></polygon>
            </g>
        </g>
    </svg>
    Add good
</a>
<br />
<button onclick=""SaveWarehouse()"" class=""alert-success"">
    Save
</button>
<script>
    function OnAddGoodLinkClick() {
        $.ajax({
            type: 'POST',
            url: ""/Warehouses/GetGood"",
            success: function (data) {
                console.log(data);
                $(""#goodsContainer"").append(data);
            }
        });
    }
    function SaveWarehouse() {
        var goods = [];
        $.each($('.good_row'), function (index, goodRow) {

            goods.push({
                ""Id"": $(goodRow).find(""input[id*='g");
            WriteLiteral(@"oodIdHidden']"").val() ? parseInt($(goodRow).find(""input[id*='goodIdHidden']"").val()) : null,
                ""GoodId"": parseInt($(goodRow).find(""select[id*='goodDropDown']"").val()),
                ""Amount"": parseInt($(goodRow).find(""input[id*='goodAmount']"").val())
            });
        });

        let model = {
            ""Id"": $(""#Id"").val() ? parseInt($(""#Id"").val()) : null,
            ""Name"": $(""#Name"").val(),
            ""Address"": $(""#Address"").val(),
            ""Goods"": goods
        };

        $.ajax({
            type: 'POST',
            data: JSON.stringify(model),
            url: ""/Warehouses/Save"",
            contentType: 'application/json',
            success: function () {
                window.location = ""/Warehouses/List""
            }
        });
    }
</script>

");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<WarehouseEditViewModel> Html { get; private set; }
    }
}
#pragma warning restore 1591
