﻿using System;
using System.Linq;
using System.Linq.Expressions;
using UnitOfWork;

namespace Repositories
{
    public class Repository<T> : IRepository<T> where T : class
    {
        TaskDbContext Context;

        public Repository(IUnitOfWork unitOfWork)
        {
            Context = unitOfWork as TaskDbContext;

            if (Context == null)
                throw new InvalidOperationException("UnitOfWork of the type DbContext should be used while a entity framework data access are used.");
        }

        public virtual T GetFirstOrDefault(Expression<Func<T, bool>> expression)
        {
            return GetAll().FirstOrDefault(expression);
        }

        protected virtual IQueryable<T> GetMainAll()
        {
            return Context.Set<T>();
        }
        public virtual IQueryable<T> GetAll()
        {
            return GetMainAll();
        }

        public IQueryable<T> FindBy(Expression<Func<T, bool>> predicate)
        {
            return GetAll().Where(predicate);
        }

        public virtual void Add(T entity)
        {
            Context.Set<T>().Add(entity);
        }

        public virtual void Delete(T entity)
        {
            Context.Set<T>().Remove(entity);
        }
    }

    public interface IRepository<T> where T : class
    {
        T GetFirstOrDefault(Expression<Func<T, bool>> expression);

        IQueryable<T> GetAll();

        IQueryable<T> FindBy(Expression<Func<T, bool>> predicate);

        void Add(T entity);

        void Delete(T entity);
    }
}
