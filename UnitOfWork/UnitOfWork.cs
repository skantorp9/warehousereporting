﻿using System;
using System.Diagnostics;
using System.Linq;
using BusinessObjects.Entities;
using BusinessObjects.Enums;
using Microsoft.EntityFrameworkCore;

namespace UnitOfWork
{
    public class TaskDbContext : DbContext, IUnitOfWork
    {
        public TaskDbContext(DbContextOptions<TaskDbContext> options) : base(options)
        {

        }
        void IUnitOfWork.SaveChanges()
        {
            try
            {
                SaveChanges();
            }
            catch (DbUpdateException e)
            {
                foreach (var eve in e.Entries)
                {
                    Debug.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entity.GetType().Name, eve.State);
                }
                throw;
            }

        }

        public DbSet<Good> Goods { get; set; }
        public DbSet<Warehouse> Warehouses { get; set; }
        public DbSet<GoodWarehouse> GoodWarehouses { get; set; }
        public DbSet<CategoryOfGood> CategoryOfGoods { get; set; }
        public DbSet<Currency> Currencies { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server=(localdb)\\mssqllocaldb;Database=TaskDb;Trusted_Connection=True;MultipleActiveResultSets=true");
        }

        public void Initialize()
        {
            if (!CategoryOfGoods.Any())
            {
                CategoryOfGoods.Add(new CategoryOfGood {  Name = CategoryId.Food.ToString() });
                CategoryOfGoods.Add(new CategoryOfGood {  Name = CategoryId.Sport.ToString() });
                CategoryOfGoods.Add(new CategoryOfGood {  Name = CategoryId.Household.ToString() });
                CategoryOfGoods.Add(new CategoryOfGood {  Name = CategoryId.Toys.ToString() });
                CategoryOfGoods.Add(new CategoryOfGood {  Name = CategoryId.Electronics.ToString() });
            }

            if (!Currencies.Any())
            {
                var date = DateTime.Today.AddDays(-1);

                Currencies.Add(new Currency { Code = "UAH", Name = "Hryvnia", ExchangeRate = 27, UpdateDate = date });
                Currencies.Add(new Currency { Code = "USD", Name = "US Dollar", ExchangeRate = 1, UpdateDate = date });
                Currencies.Add(new Currency { Code = "EUR", Name = "Euro", ExchangeRate = 0.93, UpdateDate = date });
            }

            base.SaveChanges();
        }
    }

    public interface IUnitOfWork
    {
        void SaveChanges();
    }
}
