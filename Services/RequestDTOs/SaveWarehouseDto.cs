﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Services.RequestDTOs
{
    public class SaveWarehouseDto
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public WarehouseGoodSaveDto[] Goods { get; set; } = new WarehouseGoodSaveDto[0];
    }
    public class WarehouseGoodSaveDto
    {
        public int? Id { get; set; }
        public int GoodId { get; set; }
        public int Amount { get; set; }
    }
}
