﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Services.RequestDTOs
{
    public class SaveGoodDto
    {
        public int? Id { get; set; }
        public double Price { get; set; }
        public string Name { get; set; }
        public int CategoryId { get; set; }
        public int CurrencyId { get; set; }
    }
}
