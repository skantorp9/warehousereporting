﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Services.RequestDTOs
{
    public class SaveCurrencyRequestDto
    {
        public string Code { get; set; }
        public double UAHExchangeRate { get; set; }
    }
}
