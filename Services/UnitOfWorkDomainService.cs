﻿using System;
using System.Collections.Generic;
using System.Text;
using UnitOfWork;

namespace Services
{
    public abstract class UnitOfWorkDomainService
    {
        protected IUnitOfWork UnitOfWork;

        protected UnitOfWorkDomainService(IUnitOfWork unitOfWork)
        {
            UnitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
        }
    }
}
